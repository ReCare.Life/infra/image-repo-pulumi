import pulumi
from pulumi_aws import ecr

resource_name_prefix = "recare"

health_record_lambda_ecr_repo = ecr.Repository(
    f"{resource_name_prefix}-health-record-lambda"
)

activity_lambda_ecr_repo = ecr.Repository(f"{resource_name_prefix}-activity-lambda")

appointment_lambda_ecr_repo = ecr.Repository(
    f"{resource_name_prefix}-appointment-lambda"
)

connect_lambda_ecr_repo = ecr.Repository(f"{resource_name_prefix}-connect-lambda")

pulumi.export("healthRecordLambdaEcrRepoName", health_record_lambda_ecr_repo.id)
pulumi.export(
    "healthRecordLambdaEcrRepoUrl", health_record_lambda_ecr_repo.repository_url
)
pulumi.export("activityLambdaEcrRepoName", activity_lambda_ecr_repo.id)
pulumi.export("activityLambdaEcrRepoUrl", activity_lambda_ecr_repo.repository_url)
pulumi.export("appointmentLambdaEcrRepoName", appointment_lambda_ecr_repo.id)
pulumi.export("appointmentLambdaEcrRepoUrl", appointment_lambda_ecr_repo.repository_url)
pulumi.export("connectLambdaEcrRepoName", connect_lambda_ecr_repo.id)
pulumi.export("connectLambdaEcrRepoUrl", connect_lambda_ecr_repo.repository_url)
